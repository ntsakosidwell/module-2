//
//Sidwell_Mkhabela_Module2_file3

void main() {
//instance of a class
var ambani = new App();

ambani.name = "Ambani Africa";
ambani.category = "Best Enterprise Solution";
ambani.year = 2021;

//print using object of class
ambani.printAppInfo();

}
//class of App
class App {
  var name ;
  String? category;
  int? year;

//function to print output
  void printAppInfo(){
    print("WINNING APP:\n");

    print(name.toUpperCase()); //transform name to all capital letters
    print("CATEGORY:$category");
    print("YEAR:$year");
  }

}