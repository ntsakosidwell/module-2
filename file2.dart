//A program that stores and Prints all the winning apps of MTN Business App of the Year awards since 2012
//Sidwell_Mkhabela_Module2_file2

void main() {
  //array of winning apps
  var winningApp = ["Ambani Africa","EasyEquities","Naked-Insurance","Khula-Ecosystem","Standard-bank-Shyft","Domestly","WumDrop","Live Inspect","SnapScan","Fnb-Banking-App"];
  var year=0;
//Sort Winning apps List by name
  winningApp.sort();

//print all the winning apps in the list
  print('The winning apps of the MTN Business App of the year Awards Since 2012:\n');

  for(var app in winningApp) {

    switch(app){
      case 'Ambani Africa':
        year = 2021;
        print("$app - $year");
        break;
      case 'EasyEquities':
        year = 2020;
        print("$app - $year");
        break;
      case 'Naked-Insurance':
        year = 2019;
        print("$app - $year");
        break;
      case 'Khula-Ecosystem':
        year = 2018;
        print("$app - $year");
        break;
      case 'Standard-bank-Shyft':
        year = 2017;
        print("$app - $year");
        break;
      case 'Domestly':
        year = 2016;
        print("$app - $year");
        break;
      case 'WumDrop':
        year = 2015;
        print("$app - $year");
        break;
      case 'Live Inspect':
        year = 2014;
        print("$app - $year");
        break;
      case 'SnapScan':
        year = 2013;
        print("$app - $year");
        break;
      case "Fnb-Banking-App":
        year = 2012;
        print("$app - $year");
        break;
      default:
        print("App not found");

    }
//print winning app by year
    switch (year) {
      case 2017 :
        print("The winning App of $year is $app");
        break;
      case 2018 :
        print("The winning App of $year is $app");
        break;
    }

  }
  //print total number of apps from array
  var totalnumberofapps = winningApp.length;

  print("\nThe total number of winning apps in array is :$totalnumberofapps");


}
